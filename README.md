# README #

### Installation helper for OpenLDAP ###

* This script helps to install OpenLDAP server

### How do I get set up? ###

Follow these how-to to run this script.

```
#!shell
sudo -s
cd /usr/local
git clone https://cyrill_gremaud@bitbucket.org/cyrill_gremaud/openldap_installer.git
chmod -R 700 openldap_installer
cd openldap_installer
./installOpenLDAP.sh
```

You can display the help of the script using the -h option

```
#!shell
./installOpenLDAP.sh -h
This script helps to install OpenLDAP server for e-SIS project. If the option -c is specified, the script clean the system

Usage:
 -c : Clean the system
 -d : Enable debugging
 -h : This help message
```

Do not hesitate to contact me for bug or features requests