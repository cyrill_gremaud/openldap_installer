#!/bin/bash
# This script helps to install OpenLDAP server for e-SIS project
#
# @Author Cyrill Gremaud (gremaudc@gmail.com)
# @Date 07.07.2015
#
# Bitbucket URL: https://bitbucket.org/cyrill_gremaud/openldap_installer
#

set -o pipefail > /dev/null 2>&1
if [[ $? -ne 0 ]]; then  echo -e "\e[31m[!]-> Warning ! Impossible to set pipefail. Don't use pipeline command in this script (cmd1 | cmd2) \e[0m"; fi

set -o nounset

# Constants
LDIF_DIR=$(pwd)"/ldifs"
PATCHES_DIR=$(pwd)"/patches"
APACHE_SSL_CONF="/etc/apache2/mods-available/ssl.conf"
APACHE_SECURITY_CONF1="/etc/apache2/conf.d/security"
APACHE_SECURITY_CONF2="/etc/apache2/conf-available/security.conf"
PHP_SECURITY="/etc/php5/apache2/php.ini"
LDAP_ESIS_SCHEMA="esis.schema"
LDAP_SCHEMA_DIR="/etc/ldap/schema"
SSL_STUFF_PATH="/etc/ssl/myStuff"

# Called in case of important error
function quit_on_error {
	echo -e "\e[41m[!]-> The program quits due to important error(s). Correct it before.\e[0m"
	echo -e "\e[41m[$(date +'%Y-%m-%d@%H:%M:%S')] Error information => $@\e[0m" >&2
	exit
}

function clean_option {
	echo -e "\e[93m[+] Cleaning the system\e[0m"
	rm -rf /tmp/*
	packagesList=( slapd* ldap-utils phpldapadmin gnutls-bin apache2 php5 ntp )
	echo -e "\e[32m[+] Warning ! Do you really want to PURGE (binary, data, configuration) ALL these packages ?: [Y=yes][N=no]\e[0m"
	printf " -> %s\n" "${packagesList[@]}"
	read -p "" askAll
	if [[ $askAll = "y" || $askAll = "Y" ]]; then
		apt-get remove --purge slapd* ldap-utils phpldapadmin gnutls-bin apache2 php5 ntp --yes > /dev/null 2>&1
	else
		for package in "${packagesList[@]}"; do
			read -p "[!] Do you really want to PURGE (binary, data, configuration) $package ?: [Y=yes][N=no] " ask
			if [[  $ask = "y" || $ask = "Y" ]]; then
				apt-get remove --purge $package --yes > /dev/null 2>&1
			fi
		done
	fi
	apt-get autoremove --yes > /dev/null 2>&1
	apt-get autoclean --yes > /dev/null 2>&1
}

# Parse arguments
while getopts ":cdh" FLAG; do
  case $FLAG in
	c)	#Clean the system by removing all installed components
		clean_option "Cleaning the system"
		exit 1
	;;
    d)  #set option "d"
		set -o xtrace
	;;
    h)  #show help
		echo ""
		echo "This script helps to install OpenLDAP server for e-SIS project. If the option -c is specified, the script clean the system"
		echo ""
		echo "Usage:"
		echo " -c : Clean the system"
		echo " -d : Enable debugging"
		echo " -h : This help message"
		echo ""
		exit 1
	;;
    \?) #unrecognized option - show help
		echo "This argument is not recognized, try -h to show help"
		exit 2
	;;
  esac
done

echo -e "\e[44m[+] This script helps to install OpenLDAP server                                      \e[0m"
echo -e "\e[44m[+] ----------------------------------------------------------------------------------\e[0m"
echo -e "\e[44m[+] Please read well each colorized text. Some things must be done manually           \e[0m"
echo -e "\e[44m[+]                                                                                   \e[0m"
echo -e "\e[44m[+] Author: Cyrill Gremaud (gremaudc@gmail.com)                                       \e[0m"
echo -e "\e[44m[+]                                                                                   \e[0m"

# Make sure that the script is run by root user
if [[ $EUID -ne 0 ]]; then quit_on_error "This script must be run as root"; fi

# Clean the system before
clean_option
rm -rf $LDIF_DIR
rm -rf $SSL_STUFF_PATH

# create needed directory
if [[ ! -d $LDIF_DIR ]]; then
	mkdir $LDIF_DIR
fi

if [[ ! -d $PATCHES_DIR ]]; then
	mkdir $PATCHES_DIR
fi

chmod -R 700 $LDIF_DIR
chmod -R 700 $PATCHES_DIR

# Update the server
echo -e "\e[93m[+] Please wait during OS update\e[0m"
dpkg --configure -a > /dev/null 2>&1
apt-get update > /dev/null 2>&1
apt-get upgrade --yes > /dev/null 2>&1
if [[ $? -ne 0 ]]; then  echo -e "\e[31m[!]-> Warning ! Problem during the update. Be careful.\e[0m"; fi

# Install some perquisites
echo -e "\e[93m[+] Please wait during Installing perquisites\e[0m"

# Fucking trick to be able to set the desired LDAP admin password
apt-get remove --purge slapd* ldap-utils phpldapadmin gnutls-bin apache2 php5 ntp --yes > /dev/null 2>&1
export DEBIAN_FRONTEND="noninteractive"
ldapAdminPassword=""
ldapAdminPassword2="-"
while [[ $ldapAdminPassword != $ldapAdminPassword2 ]]; do
	read -p "Please choose a LDAP administrator password: " ldapAdminPassword
	read -p "Retype your desired password: " ldapAdminPassword2
done
debconf-set-selections <<< "slapd slapd/password1 password "$ldapAdminPassword
debconf-set-selections <<< "slapd slapd/password2 password "$ldapAdminPassword
apt-get install slapd ldap-utils phpldapadmin apache2 php5 --yes > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check the installation of perquisites"; fi

# Disabled anonymous reading
echo -e "\e[93m[+] Disable anonymous reading on LDAP frontend database\e[0m"
cat > $LDIF_DIR/01_disableAnonReading.ldif <<EOL
dn: cn=config
changetype: modify
add: olcDisallows
olcDisallows: bind_anon
-

dn: olcDatabase={-1}frontend,cn=config
changetype: modify
add: olcRequires
olcRequires: authc
EOL
ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/01_disableAnonReading.ldif > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check 01_disableAnonReading.ldif for error"; fi

# Configure Logging
echo -e "\e[93m[+] Configure LDAP logging to stats\e[0m"
cat > $LDIF_DIR/02_configureLogging.ldif <<EOL
dn: cn=config
changetype: modify
replace: olcLogLevel
olcLogLevel: stats
EOL
ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/02_configureLogging.ldif > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check 02_configureLogging.ldif for error"; fi

# Set rootDN and password for olcDatabase={0}config
echo -e "\e[93m[+] Set root DN and password for olcDatabase={0}config\e[0m"
rootDnPassword=""
rootDnPassword2="-"
while [[ $rootDnPassword != $rootDnPassword2 ]]; do
	read -p "Please choose a root DN password: " rootDnPassword
	read -p "Retype your desired password: " rootDnPassword2
done
rootDnPasswordHash=$(slappasswd -s $rootDnPassword)
cat > $LDIF_DIR/03_setConfigAccess.ldif <<EOL
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootDN
olcRootDN: cn=admin,cn=config
-
add: olcRootPW
olcRootPW: $rootDnPasswordHash
EOL
ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/03_setConfigAccess.ldif > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check 03_setConfigAccess.ldif for error"; fi

# Modify rootDN and password for olcDatabase={1}hdb
echo -e "\e[93m[+] Modify root DN and password for olcDatabase={1}hdb\e[0m"
cat > $LDIF_DIR/04_setHdbAccess.ldif <<EOL
dn: olcDatabase={1}hdb,cn=config
changetype: modify
replace: olcSuffix
olcSuffix: dc=e-sis,dc=org
-
replace: olcRootDN
olcRootDN: cn=admin,dc=e-sis,dc=org
-
replace: olcRootPW
olcRootPW: $rootDnPasswordHash
EOL
ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/04_setHdbAccess.ldif > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Please check 04_setHdbAccess.ldif for error"; fi

# Configure phpLdapAdmin
echo -e "\e[93m[+] Configure phpLdapAdmin\e[0m"
patch /etc/phpldapadmin/config.php < $PATCHES_DIR/phpldapadminConfig.patch > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during applying patch on /etc/phpldapadmin/config.php"; fi

# Restart apache2
echo -e "\e[93m[+] Restarting Apache\e[0m"
/etc/init.d/apache2 restart > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during restarting apache2"; fi
echo -e "\e[32m[+] Try to access to phpldapadmin -> http://yourServerIpAddress/phpldapadmin\e[0m"

# Configure TLS on the server
echo -e "\e[93m[+] Installation of needed stuff for TLS\e[0m"
apt-get install gnutls-bin > /dev/null 2>&1
echo -e "\e[32m[+] To configure LDAP for TLS support, you need a valid server PEM certificate and the corresponding PEM key file\e[0m"
read -p "Do you want to configure LDAP for TLS now ?[Y=yes][N=no]" answer
if [[ $answer = "Y" || $answer = "y" ]]; then

	if [[ ! -d $SSL_STUFF_PATH ]]; then
		mkdir $SSL_STUFF_PATH
	fi

	echo -e "\e[32m[!] Please move CA, server certificat and server private key into $SSL_STUFF_PATH\e[0m"

	read -p "What is the name of server certificate PEM file ?: " certPath
	while [[ ! -f $SSL_STUFF_PATH/$certPath ]]; do
		echo -e "\e[31m[!]-> Server certificate not found ($SSL_STUFF_PATH/$certPath)\e[0m"
		read -p "What is the name of server certificate PEM file ?: " certPath
	done

	read -p "Where is the name of server private key PEM file ?: " keyPath
	while [[ ! -f $SSL_STUFF_PATH/$keyPath ]]; do
		echo -e "\e[31m[!]-> Server private key not found ($SSL_STUFF_PATH/$keyPath)\e[0m"
		read -p "What is the name of server private key PEM file ?: " keyPath
	done

	read -p "What is the name of CA certificate PEM file ?: " caCertPath
	while [[ ! -f $SSL_STUFF_PATH/$caCertPath ]]; do
		echo -e "\e[31m[!]-> CA certificate not found ($SSL_STUFF_PATH/$caCertPath)\e[0m"
		read -p "What is the name of CA certificate PEM file ?: " caCertPath
	done

	# check permissions on folders for security reason
	echo -e "\e[32m[+] Checking permissions\e[0m"
	chmod 400 $SSL_STUFF_PATH/$certPath
	chmod 400 $SSL_STUFF_PATH/$keyPath
	chmod 400 $SSL_STUFF_PATH/$caCertPath

	# Configure slapd to use certificates
	if [[ ! -d $LDIF_DIR/tls ]]; then
                mkdir $LDIF_DIR/tls
        fi

	echo -e "\e[32m[+] Adding TLS configuration to slapd\e[0m"
	cat > $LDIF_DIR/tls/01_addTlsConfig.ldif <<EOL
dn: cn=config
add: olcTLSCACertificateFile
olcTLSCACertificateFile: $SSL_STUFF_PATH/$caCertPath
-
add: olcTLSCertificateFile
olcTLSCertificateFile: $SSL_STUFF_PATH/$certPath
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: $SSL_STUFF_PATH/$keyPath
EOL
	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/tls/01_addTlsConfig.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check tls/01_addTlsConfig.ldif for error"; fi

	echo -e "\e[93m[+] Secure access to server private key\e[0m"
	adduser openldap ssl-cert > /dev/null 2>&1
	chgrp ssl-cert $SSL_STUFF_PATH/$keyPath > /dev/null 2>&1
	chmod g+r $SSL_STUFF_PATH/$keyPath
	chmod o-r $SSL_STUFF_PATH/$keyPath

	echo -e "\e[93m[+] Configure appArmor to allow slapd to read private key\e[0m"
	if [[ ! -f /etc/apparmor.d/usr.sbin.slapd.bkp ]]; then
		cp /etc/apparmor.d/usr.sbin.slapd /etc/apparmor.d/usr.sbin.slapd.bkp
	fi

	#add path at the end of apparmor slapd configuration
	sed -i '$ i\'"$SSL_STUFF_PATH"'/ r,' /etc/apparmor.d/usr.sbin.slapd
	sed -i '$ i\'"$SSL_STUFF_PATH"'/* r,' /etc/apparmor.d/usr.sbin.slapd

	echo -e "\e[93m[+] Restarting appArmor to check configuration\e[0m"
	service apparmor restart > /dev/null 2>&1

	echo -e "\e[93m[+] Restarting openLDAP to check configuration\e[0m"
	/etc/init.d/slapd restart
        if [[ $? -ne 0 ]]; then
                echo -e "\e[31m[!]-> Cannot restart slapd. Please be sure that appArmor is configured correctly.  check the syslog";
                read -p "Press Enter to show slapd log"
                cat /var/log/syslog | egrep "slapd"
        fi

	echo -e "\e[93m[+] Configuring /etc/ldap/ldap.conf\e[0m"
	#todo maybe if incoming connection from clients are blocked
fi

# Disabled SSLv2 and SSLv3
echo -e "\e[93m[+] Secure Apache server against POODLE attack\e[0m"
if [[ ! -f $APACHE_SSL_CONF ]]; then
	echo -e "\e[31m[!]-> Warning ! Impossible to find $APACHE_SSL_CONF. Please check how to configure apache SSL with this version \e[0m"
	apacheInfos=$(apache2 -v)
	echo -e "\e[31m[!]-> $apacheInfos\e[0m"
else
	sed -i 's/SSLProtocol\sall/SSLProtocol\sall\s-SSLv2\s-SSLv3/' $APACHE_SSL_CONF
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching apache2 against POODLE attack"; fi
fi
apachectl configtest > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during checking apache2 configuration. patching POODLE problem ?"; fi

# Avoid PHP and Apache server information leakage
echo -e "\e[93m[+] Secure PHP and Apache server against information leakage\e[0m"
if [[ -f $APACHE_SECURITY_CONF1 ]]; then
	sed -i 's/ServerTokens\sOS/ServerTokens Prod/' $APACHE_SECURITY_CONF1
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching apache2 against information leakage (ServerToken)"; fi
	sed -i 's/ServerSignature\sOn/ServerSignature Off/' $APACHE_SECURITY_CONF1
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching apache2 against information leakage (ServerSignature)"; fi
elif [[ -f $APACHE_SECURITY_CONF2 ]]; then
	sed -i 's/ServerTokens\sOS/ServerTokens Prod/' $APACHE_SECURITY_CONF2
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching apache2 against information leakage (ServerToken)"; fi
	sed -i 's/ServerSignature\sOn/ServerSignature Off/' $APACHE_SECURITY_CONF2
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching apache2 against information leakage (ServerSignature)"; fi
else
	quit_on_error "Error while securing Apache against information leakage. $APACHE_SECURITY_CONF1 or APACHE_SECURITY_CONF2 are both not present"
fi

if [[ -f $PHP_SECURITY ]]; then
	sed -i 's/expose_php\s=\sOn/expose_php = Off/' $PHP_SECURITY
	if [[ $? -ne 0 ]]; then quit_on_error "Error during patching PHP against information leakage (expose_php)"; fi
else
	quit_on_error "Error while securing PHP for Apache against information leakage. $PHP_SECURITY is not present"
fi

/etc/init.d/apache2 restart > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during restarting apache2 (after information leakage modification)"; fi

#Creating the e-SIS custom scheme
echo -e "\e[93m[+] Creating the e-SIS custom scheme\e[0m"
cp $LDAP_ESIS_SCHEMA $LDAP_SCHEMA_DIR
if [[ $? -ne 0 ]]; then quit_on_error "Error during copying esis.schema. Please check if $LDAP_ESIS_SCHEMA is present"; fi

mkdir /tmp/ldapworkingdir
echo "include $LDAP_SCHEMA_DIR/$LDAP_ESIS_SCHEMA" > /tmp/ldapworkingdir/ldap.conf
slaptest -f /tmp/ldapworkingdir/ldap.conf -F /tmp/ldapworkingdir > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during converting $LDAP_ESIS_SCHEMA into ldif file"; fi

if [[ ! -f "/tmp/ldapworkingdir/cn=config.ldif" || ! -d "/tmp/ldapworkingdir/cn=config" ]]; then
	quit_on_error "Error. Please check that cn=config.ldif and cn=config directory are present in /tmp/ldapworkingdir"
fi

# modify some content of the file for fun (or not)
sed -i 's/dn:\scn={0}esis/dn: cn=esis,cn=schema,cn=config/' "/tmp/ldapworkingdir/cn=config/cn=schema/cn={0}esis.ldif"
sed -i 's/cn:\s{0}esis/cn:esis/' "/tmp/ldapworkingdir/cn=config/cn=schema/cn={0}esis.ldif"

# remove the last lines of the file...
head --lines=-7 "/tmp/ldapworkingdir/cn=config/cn=schema/cn={0}esis.ldif" > "/tmp/ldapworkingdir/cn=config/cn=schema/cn={0}esis.ldif.parsed"

#Adding the e-SIS custom scheme to ldap
echo -e "\e[93m[+] Adding the e-SIS custom scheme to ldap\e[0m"
ldapadd -Q -Y EXTERNAL -H ldapi:/// -f "/tmp/ldapworkingdir/cn=config/cn=schema/cn={0}esis.ldif.parsed" > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Error during adding e-sis custom schema to LDAP server"; fi

#Be sure that the new schema has been really added to ldap configuration
ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config * | egrep "esis,cn=schema,cn=config" > /dev/null 2>&1
if [[ $? -ne 0 ]]; then quit_on_error "Strange error. Please be sure that new schema is correctly added to LDAP configuration"; fi

read -p "Do you want to configure replication now ? [Y=yes][N=no]" answer
if [[ "$answer" = "y" || "$answer" = "yes" || "$answer" = "Y" ]]; then
	### Optional installation of replication configuration ###
	echo -e "\e[93m[+] installation of replication configuration\e[0m"
	echo -e "\e[32m[+] Installing and configuring NTP to use Swiss servers\e[0m"
	apt-get install ntp --yes > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Error during the installation of NTP package"; fi

	sed -i 's/server\s0.ubuntu.pool.ntp.org/server 0.ch.pool.ntp.org iburst/' /etc/ntp.conf
	sed -i 's/server\s1.ubuntu.pool.ntp.org/server 1.ch.pool.ntp.org iburst/' /etc/ntp.conf
	sed -i 's/server\s2.ubuntu.pool.ntp.org/server 2.ch.pool.ntp.org iburst/' /etc/ntp.conf
	sed -i 's/server\s3.ubuntu.pool.ntp.org/server 3.ch.pool.ntp.org iburst/' /etc/ntp.conf

	echo -e "\e[32m[+] Try to restart NTP service\e[0m"
	/etc/init.d/ntp restart > /dev/null 2>&1
	sleep 2
	if [[ -f /var/run/ntpd.pid ]]; then
		pid=$(cat /var/run/ntpd.pid)
		echo -e "\e[32m[!]-> NTP restarted successfully with pid $pid\e[0m"
	else
		echo -e "\e[31m[!]-> NTP seems not started. Please check the syslog\e[0m"
		read -p "Press Enter to show ntp log"
		cat /var/log/syslog | egrep "ntpd"
	fi

	#work for the moment but with a workaround... not very sexy. to fix (bitbucket Issue #3)
	echo -e "\e[32m[+] Modifying /etc/default/slapd configuration file\e[0m"
	read -p "LDAP server public IP: " publicIP
	#read -p "LDAP server DNS name: " dnsname
	echo "SLAPD_SERVICES='"ldap://$publicIP ldapi:///"'" >> /etc/default/slapd
	#sed -i 's/SLAPD_SERVICES="ldap:\/\/\/\sldapi:\/\/\/"/SLAPD_SERVICES="ldap://$publicIP ldap://$dnsname ldapi:///"/' /etc/default/slapd
	if [[ $? -ne 0 ]]; then quit_on_error "Error configuring /etc/default/slapd (SLAPD_SERVICES variable)"; fi

	/etc/init.d/slapd restart
	if [[ $? -ne 0 ]]; then 
		echo -e "\e[31m[!]-> Cannot restart slapd. Please be sure that IP and DNS name are correct. check the syslog";
		read -p "Press Enter to show slapd log"
		cat /var/log/syslog | egrep "slapd"
	fi

	echo -e "\e[32m[+] Configuring cn=config replication\e[0m"
	if [[ ! -d $LDIF_DIR/replication ]]; then
		mkdir $LDIF_DIR/replication
	fi

	echo -e "\e[32m[+] Load syncprov module to cn=config\e[0m"
	cat > $LDIF_DIR/replication/01_loadSyncprovModule.ldif <<EOL
	dn: cn=module{0},cn=config
	changetype: modify
	add: olcModuleLoad
	olcModuleLoad: syncprov
EOL

	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/replication/01_loadSyncprovModule.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check replication/01_loadSyncprovModule.ldif for error"; fi

	echo -e "\e[32m[+] Set the serverID to cn=config\e[0m"
	#read -p "Enter a ID for this server. (must be unique): " serverID
	cat > $LDIF_DIR/replication/02_setServerID.ldif <<EOL
	dn: cn=config
	changetype: modify
	add: olcServerID
	olcServerID: 1
EOL

	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/replication/02_setServerID.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check replication/02_setServerID.ldif for error"; fi

	echo -e "\e[32m[+] Set all server IDs\e[0m"
	read -p "How many LDAP must be synchronized? " number
	counter=1
	sids[0]=0
	surls[0]=0
	ldifInfo=$'dn: cn=config\nchangetype: modify\nreplace: olcServerID\n'
	while [[ $counter -ne $number+1 ]]; do
		#read -p "Unique server ID for server $counter: " sids[$counter]
		sids[$counter-1]=$counter
		read -p "DNS name or public IP address for server $counter: " surls[$counter-1]
		ldifInfo="$ldifInfo"$''"olcServerID: ${sids[$counter-1]} ldap://${surls[$counter-1]}"$'\n'
		let counter=counter+1
	done

	cat > $LDIF_DIR/replication/03_allServerID.ldif <<EOL
	$ldifInfo
EOL
	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/replication/03_allServerID.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check replication/03_allServerID.ldif for error"; fi

	echo -e "\e[32m[+] Add syncprov module to the overlay configuration\e[0m"
	cat > $LDIF_DIR/replication/04_addSyncProvToOverlay.ldif <<EOL
	dn: olcOverlay=syncprov,olcDatabase={0}config,cn=config
	changetype: add
	objectClass: olcOverlayConfig
	objectClass: olcSyncProvConfig
	olcOverlay: syncprov
EOL
	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/replication/04_addSyncProvToOverlay.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check replication/04_addSyncProvToOverlay.ldif for error"; fi

	echo -e "\e[32m[+] Add SyncRepl between the servers\e[0m"
	counter=0
	ldifInfo2=$'dn: olcDatabase={0}config,cn=config\nchangetype: modify\nadd: olcSyncRepl\n'
	while [[ $counter -lt $number ]]; do
		rid=0
		let rid=counter+1
		if [[ $counter -eq $number-1 ]]; then
			ldifInfo2="$ldifInfo2"$''"olcSyncRepl: rid=00$rid provider=ldap://${surls[$counter]} binddn="'"cn=admin,cn=config"'" bindmethod=simple credentials=$rootDnPassword searchbase="'"cn=config"'" type=refreshAndPersist retry="'"5 +"'" timeout=1"
		else
			ldifInfo2="$ldifInfo2"$''"olcSyncRepl: rid=00$rid provider=ldap://${surls[$counter]} binddn="'"cn=admin,cn=config"'" bindmethod=simple credentials=$rootDnPassword searchbase="'"cn=config"'" type=refreshAndPersist retry="'"5 +"'" timeout=1"$'\n'
		fi
		let counter=counter+1
	done

	cat > $LDIF_DIR/replication/05_addSyncRepl.ldif <<EOL
$ldifInfo2
-
add: olcMirrorMode
olcMirrorMode: TRUE
EOL

	ldapmodify -Y EXTERNAL -H ldapi:// -f $LDIF_DIR/replication/05_addSyncRepl.ldif > /dev/null 2>&1
	if [[ $? -ne 0 ]]; then quit_on_error "Please check replication/05_addSyncRepl.ldif for error"; fi

fi

echo -e "\e[93m[+] Installation done\e[0m"

# Clean the generated stuff
echo -e "\e[93m[+] Cleaning the generated stuff\e[0m"
rm -rf /tmp/*
